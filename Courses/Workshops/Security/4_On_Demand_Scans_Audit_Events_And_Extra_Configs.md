### We will wrap up the last few security capabilities that GitLab offers beyond what is possible within our CI/CD configuration by adding on demand scans, audit events, and further configurations for our scans

### Theme

In case of security risks events these tools are extremely handy at quickly troubleshooting what may be the issue with your application.

# Step 1: Set Up an On Demand Scan

1. Using the left hand navigation menu click through **Secure > On-demand scans**. We then want to click **New scan**.
  
2. In the ***Scan configuration*** section add a name and description but leave the rest blank.
  
3. Next in the ***DAST configuration*** section click **Select scanner profile** then **New scanner profile**, give it a name, leave the rest as is and click **Save profile**.
  
4. We then want to click **Select site profile** followed by **New site profile**. Give it a name an under ***Target URL*** enter http://www.example.com/home. It is out of scope to fully deploy out this application therefore the DAST scanner will not run, but think about how you could customize these settings for your own application.
  
5. Then click **Save profile** and scroll down to **Save scan**.
  
6. Your presenter can also show you what this scan looks like on a live [Tanuki Racing deployment](https://gitlab.com/gitlab-learn-labs/webinars/tanuki-racing/tanuki-racing-application/-/on_demand_scans#/all)

> [Docs on DAST Scanning](https://docs.gitlab.com/ee/user/application_security/dast/)

# Step 2: Project Audit Events

1. Using the left hand navigation menu click through **Secure > Audit events** to get the report on all actions taken on the project for the past month. If no events are shown you may need to edit the time frame.
  
2. You should be able to see some of the actions you have taken and where they occurred in the project hierarchy

# Step 3: Extra Scanner Configuration

1. Using the left hand navigation menu click through **Secure > Security configuration** to see all of the scans we have enabled. For this exercise we want to click **Configure SAST**
  
2. Take note of all of the various settings we can change about the scan to better suit our needs. 
  
3. Most of these settings can also be changed by adding variables to your pipeline, but often it is easier to change from this view.
  
4. Still within the ***Security Configuration*** settings we want to click the **Vulnerability Management** tab and enable the security training providers. Now our Vulnerability will link out to certain security trainings if they are related to an existing course. **_Please note some of the training courses do require a subscription_**
  
# Step 4: CODEOWNERS

1. Another compliance feature you should take advantage of is the use of the CODEOWNERS file. To do this we will first navigate back to the main page of our project and click **Web IDE**. Once in the IDE create a new file called ***CODEOWNERS***
  
2. Add the code below to the file. **MAKE SURE YOU REPLACE GITLAB ID WITH YOUR ID**:
    ```
    [Infrastructure]
    *.gemspec @gitlab_id
    ```

3. Now we want to commit this code to main. Go ahead and click the **Source Control** button on the left hand side, add a commit message then click **Commit & Push**. Next on the resulting dropdown make sure you click commit to main, then on the popup click the **Go to project** button. 

> [Docs for CodeOwners](https://docs.gitlab.com/ee/user/project/code_owners.html)

# Step 5: License Compliance and CODEOWNERS In Action

1. Now we want to see License Compliance and CODEOWNERS working in action. From the main page of our project lets go ahead and click **Web IDE** in the **Edit** dropdown list.
    
2. Next locate the **tanuki-racing.gemspec** file. On line 22 you can see that there are a number of runtime dependencies listed. Your project manager has expressed interest speeding up some of our jobs using sidekiq so you decide to check it out:

    ```
    s.add_runtime_dependency 'sidekiq', '~> 7.2'
    ```

3. We then also want to manually replace the **gemfile.lock** file content to include all of the changes that a _bundle install_ with the sidekiq gem would require:

```
PATH
  remote: .
  specs:
    tanukiracing (0.0.1)
      activerecord (~> 7.0.8)
      sequel (~> 5.72.0)
      sidekiq (~> 7.2)
      sinatra (~> 3.1.0)
      sqlite3 (~> 1.6.6)
      test-unit (~> 3.6.1)
      webrick (~> 1.8.1)

GEM
  remote: http://rubygems.org/
  specs:
    activemodel (7.0.8.1)
      activesupport (= 7.0.8.1)
    activerecord (7.0.8.1)
      activemodel (= 7.0.8.1)
      activesupport (= 7.0.8.1)
    activesupport (7.0.8.1)
      concurrent-ruby (~> 1.0, >= 1.0.2)
      i18n (>= 1.6, < 2)
      minitest (>= 5.1)
      tzinfo (~> 2.0)
    ast (2.4.2)
    base64 (0.1.1)
    bigdecimal (3.1.6)
    concurrent-ruby (1.2.3)
    connection_pool (2.4.1)
    i18n (1.14.1)
      concurrent-ruby (~> 1.0)
    json (2.7.1)
    language_server-protocol (3.17.0.3)
    minitest (5.22.2)
    mustermann (3.0.0)
      ruby2_keywords (~> 0.0.1)
    parallel (1.24.0)
    parser (3.3.0.5)
      ast (~> 2.4.1)
      racc
    power_assert (2.0.3)
    racc (1.7.3)
    rack (2.2.8.1)
    rack-protection (3.1.0)
      rack (~> 2.2, >= 2.2.4)
    rack-test (2.1.0)
      rack (>= 1.3)
    rainbow (3.1.1)
    rake (13.0.6)
    redis-client (0.20.0)
      connection_pool
    regexp_parser (2.9.0)
    rexml (3.2.6)
    rubocop (1.56.4)
      base64 (~> 0.1.1)
      json (~> 2.3)
      language_server-protocol (>= 3.17.0)
      parallel (~> 1.10)
      parser (>= 3.2.2.3)
      rainbow (>= 2.2.2, < 4.0)
      regexp_parser (>= 1.8, < 3.0)
      rexml (>= 3.2.5, < 4.0)
      rubocop-ast (>= 1.28.1, < 2.0)
      ruby-progressbar (~> 1.7)
      unicode-display_width (>= 2.4.0, < 3.0)
    rubocop-ast (1.30.0)
      parser (>= 3.2.1.0)
    ruby-progressbar (1.13.0)
    ruby2_keywords (0.0.5)
    sequel (5.72.0)
      bigdecimal
    sidekiq (7.2.2)
      concurrent-ruby (< 2)
      connection_pool (>= 2.3.0)
      rack (>= 2.2.4)
      redis-client (>= 0.19.0)
    sinatra (3.1.0)
      mustermann (~> 3.0)
      rack (~> 2.2, >= 2.2.4)
      rack-protection (= 3.1.0)
      tilt (~> 2.0)
    sqlite3 (1.6.9-arm64-darwin)
    test-unit (3.6.2)
      power_assert
    tilt (2.3.0)
    tzinfo (2.0.6)
      concurrent-ruby (~> 1.0)
    unicode-display_width (2.5.0)
    webrick (1.8.1)

PLATFORMS
  arm64-darwin-22
  x86_64-linux

DEPENDENCIES
  rack (~> 2.2.8)
  rack-test (~> 2.1.0)
  rake (~> 13.0.6)
  rubocop (~> 1.56.3)
  tanukiracing!

BUNDLED WITH
   2.3.7
```

4. Once added click the source control button on the left hand side, add a quick commit message, then click the **down arrow**.
  
5. On the resulting drop down click **Yes** to open a new branch, then click the **_Enter_** key. A new popup will appear where we want to then click **Create MR**

6. Scroll to the bottom, uncheck **_Delete source branch when merge request is accepted_**, and click **Create merge request**
  
7. On the resulting MR notice that our policy requires approval from **_lfstucker_** and is blocked by our two policies before we are able to merge. Wait for the entire pipeline to finish running.

8. Once done running you can see that our CODEOWNERS protection rule and License Compliance policy have been enacted restricting us from committing this code. If we wouldn't have used a blocked license or modified a file protected by CODEOWNERS we now would be able to merge our code.